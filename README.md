# jflyfox_base
common tools,StrUtils,NumberUtils,DateUtils etc.

# Author
georgefox ([@georgefox](https://github.com/jflyfox))<br/>
mail: georgefox2016@hotmail.com

# support
* alipay QR code

![jflyfox](http://ww1.sinaimg.cn/mw690/3fc7e281jw1eqec436tzwj2074074mxr.jpg "Open source support(alipay)")

* weixin QR code

![jflyfox](http://ww1.sinaimg.cn/mw690/3fc7e281jw1es3jr0k25xj20a50a5q3v.jpg "Open source support(weixin)")